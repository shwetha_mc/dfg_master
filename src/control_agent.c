#include <stdio.h>

#include "control_agent.h"
//#include "cw_mon.h"
//#include "metrics.h"
#include "ctrl_init.h"
//#include "ctrl_utils.h"
#include "pthread.h"
#include "fcntl.h"

struct timeval start_ctrl, end_ctrl;
struct timeval send_ctrl, recv_ctrl;

static EVclient test_client_sink,test_client;

static int simple_handler(CManager cm, void *vevent, void *client_data, attr_list attrs)
{
	gettimeofday(&start_ctrl,NULL);
	mon_rec_ptr event = (mon_rec_ptr) vevent;
//	EVsubmit(src_ctrl,event,NULL);
//	EVfree_source(src_ctrl);
	//float cpu_util = 0.1;
	
	float cpu_util = event->cpu_util;
	int rangebins = event->rangebins;
	event->rangebins = 1000;
	 printf("Usage is %f rangebins %d \n",event->cpu_util,event->rangebins);
	
	func(event);			 
	
	gettimeofday(&end_ctrl,NULL);
	printf("Time to process control event end %ld start %ld\n",(end_ctrl.tv_usec + (end_ctrl.tv_sec * 1000000)),(start_ctrl.tv_usec + (start_ctrl.tv_sec * 1000000)));
	printf("Usage is %f mem is %ld \n",event->cpu_util,event->mem);
	 gettimeofday(&send_ctrl,NULL);
	  printf("Sending ctrl msg at %ld\n",(send_ctrl.tv_usec + (send_ctrl.tv_sec * 1000000)));

	//EVclient_shutdown(test_client_sink,0);
	return 1;
}


static int pong_handler(CManager cm, void *vevent, void *client_data, attr_list attrs)
{
	gettimeofday(&recv_ctrl,NULL);
	printf("Cov pinged back at %ld\n",(recv_ctrl.tv_usec + (recv_ctrl.tv_sec * 1000000)));
	return 1;
}


/*static int pong_handler_mon(CManager cm, void *vevent, void *client_data, attr_list attrs)
{
	struct timeval mon_end;
	mon_rec_ptr event = (mon_rec_ptr) vevent;
	gettimeofday(&mon_end,NULL);
	printf("Heard back from control at %ld\n",(mon_end.tv_usec + (mon_end.tv_sec * 1000000)));
	return 1;
	
}*/

static int app_spy_handler(CManager cm, void *vevent, void *client_data, attr_list attrs)
{

	mon_app_ptr event = vevent;
	
	get_mon_data(&mon_data, event);
	//EVclient_shutdown(app_sink,0);
	return 1;
}



int main(int argc, char *argv[])
{
	int ret;
	int i;
	char nodename[20], color;
	char mastercontact[2048];
	CManager cm;
	char *chandle;
	struct timeval startmon;
	EVclient_sinks sink_capabilities;
	EVclient_sources source_capabilities;
	cm=CManager_create();
	printf("%d\n",__LINE__);
	if(argv[1] && argv[2] && argv[3])
	{
		printf("%d\n",__LINE__);

		sscanf(argv[1],"%s",nodename);
		sscanf(argv[2],"%c",&color);
		sscanf(argv[3],"%s",&mastercontact[0]);
		printf("%d %s %c %s\n",__LINE__,nodename, color, mastercontact);
	}
	else
	printf("Usage: ./control nodename w/b mastercontact\n where w is a source and b is a sink");
	
	
	if(color == 'w')
	{
		mon_rec mon_data;
		mon_app_ptr event;
		get_mon_data(&mon_data,event);
		EVsource src;
	//	EVclient test_client;
		printf("%d\n",__LINE__);
		char sinkname[]="sink_a_spy";
		char sourcename[]="src_";
		strcat(sourcename,nodename);


		src = EVcreate_submit_handle(cm,DFG_SOURCE,mon_rec_format_list);
		source_capabilities = EVclient_register_source(sourcename,src);

		

	//	test_client = EVclient_assoc(cm,nodename,mastercontact,source_capabilities,NULL);

		sink_capabilities = EVclient_register_sink_handler(cm, sinkname, mon_app_spy_format_list, (EVSimpleHandlerFunc) app_spy_handler, NULL);

		test_client = EVclient_assoc(cm,nodename,mastercontact,source_capabilities,sink_capabilities);
	//	 printf("%d\n",__LINE__);

	//	test_client = EVclient_assoc(cm,nodename,mastercontact,src_app_capabilities,sink_ctrl_cap);
		EVclient_ready_wait(test_client);

	//	printf("%d\n",EVclient_ready_wait(test_client));
		if(EVclient_source_active(src)) {
		EVsubmit(src,&mon_data,NULL);
		}
	//	EVfree_source(src);
		EVclient_ready_for_shutdown(test_client);
		EVclient_wait_for_shutdown(test_client);
		
	}
	else
	{

		init();
		func = (long (*)()) gen_code->func;
		char sinkname[]="sink_";
		char srcname[] = "src_";
		char sink_dum[]= "sink_ctrl_pong";		

		EVsource ctrl_src;
		EVclient_sources ctrl_src_cap;
	
		EVclient_sinks ctrl_sink_cap;		

		strcat(sinkname,nodename);
		strcat(srcname,nodename);
		
		ctrl_src=EVcreate_submit_handle(cm,DFG_SOURCE,ctrl_msg_rec_format_list);
		ctrl_src_cap =  EVclient_register_source(srcname,ctrl_src);


		sink_capabilities = EVclient_register_sink_handler(cm, sinkname, mon_rec_format_list, (EVSimpleHandlerFunc) simple_handler, NULL);
		ctrl_sink_cap = EVclient_register_sink_handler(cm, sink_dum, ctrl_msg_rec_format_list, (EVSimpleHandlerFunc) pong_handler, NULL);

		test_client_sink = EVclient_assoc(cm,nodename,mastercontact,ctrl_src_cap,sink_capabilities);
		test_client_sink = EVclient_assoc(cm,nodename,mastercontact,NULL,ctrl_sink_cap);
		EVclient_ready_wait(test_client_sink);
		ctrl_msg_dat.ctrl_ev = 0;
		ctrl_msg_dat.payload = 2;
		 if(EVclient_source_active(ctrl_src)) {
//			gettimeofday(&send_ctrl,NULL);
	                EVsubmit(ctrl_src,&ctrl_msg_dat,NULL);
//			printf("Sending ctrl msg at %ld\n",(send_ctrl.tv_usec + (send_ctrl.tv_sec * 1000000)));		
                }
		

		if(EVclient_active_sink_count(test_client_sink)==0) {
		cleanup();		
		EVclient_ready_for_shutdown(test_client_sink); }
		EVclient_wait_for_shutdown(test_client_sink);
		
			

	}

}
