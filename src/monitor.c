#include <stdlib.h>
#include <stdio.h>
#include "mon.h"

void get_mon_data(mon_rec *mon_data, mon_app_data* mon_app_data)
{
	int n, num_cores, i;
	num_cores = num_cpustates_func();
        n = num_cores + 1;
        double usage[n];
	gettimeofday(&start_hoststate,NULL);
	printf("Start getting host info at %ld usec \n",(start_hoststate.tv_usec + (start_hoststate.tv_sec * 1000000)));
	cpu_and_core_usage_func(usage);
	mon_data->cpu_util=usage[0];
	mon_data->mem=vm_mem_free();
	printf("Testing net data Receive %lld Transmit %lld\n",received_bytes("eth0"),sent_bytes("eth0"));
	gettimeofday(&end_hs,NULL);
//	long timeelapsed;
//	timeelapsed = (end_hs.tv_sec - start_hoststate.tv_sec)*1000000 + (end_hs.tv_usec - start_hoststate.tv_usec);
	printf("End get host state %ld \n ",(end_hs.tv_usec + (end_hs.tv_sec * 1000000)));
	mon_data->rangebins=mon_app_data->rangebins;
	mon_data->oversample=8;//mon_app_data->oversample;
	
	
}
