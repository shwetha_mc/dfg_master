#include "parser.h"



int parser()
{
     char token[20];
     FILE *rules;
     rules = fopen(rulesfile,"r");

     int ret=0;
     char t_high[10], t_low[10];
	
     char cond[100], func[100];
     ctrl_instance = malloc(sizeof(control)*2);
     countinstances = 0;
     if(ctrl_instance!=NULL)
     {	
	while(fscanf(rules,"%s",token)!=EOF)
	{

		if(strcmp(token,"ctrl_BEGIN")==0)
		{

			if(countinstances >= 1) {
				ctrl_instance = realloc(ctrl_instance,(countinstances+1)*sizeof(control));
			}
			fscanf(rules,"%s",token);
			int i=1;
			while(strcmp(token,ctrl_EVENT_ID_ARR[i++])!=0 && i<=NUMEVENTS);

			if(i<=NUMEVENTS)
			{	
				ctrl_instance[countinstances].event=i-1;
				
				fscanf(rules,"%s\n",func);
				ctrl_instance[countinstances].function=strdup((const char *)func);
				fscanf(rules,"%s ",token);		
				*cond = '\0';
				while(strcmp(token,"ctrl_END")!=0)
				{
					strcat(cond,(const char *)token);
					strcat(cond," >= ");
					fscanf(rules,"%s %s ",t_low,t_high);
					strcat(cond,(const char*)t_low);
					strcat(cond," && ");
					strcat(cond,token);
					strcat(cond," <= ");
					strcat(cond,(const char *)t_high);
					strcat(cond," ");
					fscanf(rules," %s",token);
					if(strcmp(token,"ctrl_END")!=0 ) {
						strcat(cond,token);
						strcat(cond," ");
						fscanf(rules,"%s ",token);
						
					}
					

				}
				
				ctrl_instance[countinstances].condition = strdup((const char *)cond);
				countinstances++;			
				ret = 1;	
			}
			else
				fprintf(stderr,"Control action not defined \n");
			

		}
		else
			fprintf(stderr,"Improperly defined rules file. \n");
	
	}
    }
    else
	fprintf(stderr,"Problems with malloc.\n");
    	
    fclose(rules);
    return ret;
}
void cleanup()
{
	free(ctrl_instance);
}
/*void main()
{
	int size,i;
	if(parser());
	
	size=(int)(sizeof(ctrl_instance)/sizeof(control));
	printf(" size %d %d %d %d\n",countinstances,sizeof(ctrl_instance),sizeof(ctrl_instance[0]),sizeof(struct _ctrl_construct));
	for(i=0;i<countinstances;++i)
		printf("ctrl_instance %d Details: Do %s if %s\n",i,ctrl_EVENT_ID_ARR[ctrl_instance[i].event],ctrl_instance[i].condition);
}*/
