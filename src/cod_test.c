#include <stdlib.h>
#include "evpath.h"
#include "cod.h"

void descope()
{
  printf("%d\n",__LINE__);
  printf("called\n");
}


void main()
{
	int rangebins=1000;
	float cpu_util=99.8;
	char cmd[] = "{\n\
		if(cpu_util > 90 && rangebins > 50) {\n\
				descope();\n\
			}\n\
		      }\n";
  	
	cod_extern_entry ext_func[] = {
		{"descope",(void*)descope},
		{NULL, NULL}
	};
	
	char ext_func_string[]= "void descope();";
	char gen_func_string[]="void proc(int rangebins, float cpu_util);";

	cod_parse_context context = new_cod_parse_context();
  	cod_code gen_code;
  	long (*func)(int rangebins, float cpu_util);
	
	//cod_subroutine_declaration(gen_func_string,context);
	
	cod_assoc_externs(context,ext_func);
	cod_parse_for_context(ext_func_string, context);
	cod_add_param("rangebins","int",0,context);
	cod_add_param("cpu_util","float",1,context);
	cod_set_return_type("void",context);
  	gen_code = cod_code_gen(cmd, context);
  	func = (long (*)()) gen_code->func;
  	func(1000, 99.8);
  	printf("%d Back here\n",__LINE__);
  	cod_free_parse_context(context);
  	cod_code_free(gen_code);
}




