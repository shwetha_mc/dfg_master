#include <stdlib.h>
#include <metrics.h>
#include <sys/time.h>

struct timeval start_hoststate, end_hs, end_appstate, elapsed;

typedef struct _mon_app_data {
	int rangebins;
	int oversample;
} mon_app_data, *mon_app_ptr;

typedef struct _mon_rec {
        double cpu_util;
        long mem;
        int rangebins;
        int oversample;
} mon_rec, *mon_rec_ptr;

mon_rec mon_data;

static FMField mon_rec_field_list[] =
{
    {"cpu_util", "double", sizeof(double), FMOffset(mon_rec_ptr, cpu_util)},
    {"mem", "double", sizeof(double), FMOffset(mon_rec_ptr, mem)},
    {"rangebins", "integer", sizeof(int), FMOffset(mon_rec_ptr, rangebins)},
    {"oversample", "integer", sizeof(int), FMOffset(mon_rec_ptr, oversample)},
    {NULL, NULL, 0, 0}
};

static FMStructDescRec mon_rec_format_list[] =
{
    {"mon_rec", mon_rec_field_list, sizeof(mon_rec), NULL},
    {NULL, NULL}
};

static FMField mon_app_data_field_list[] =
{
	{"rangebins","integer",sizeof(int), FMOffset(mon_app_ptr, rangebins)},
	 {"oversample","integer",sizeof(int), FMOffset(mon_app_ptr, oversample)},
	 {NULL, NULL, 0, 0}
};

static FMStructDescRec mon_app_spy_format_list[] =
{
	{"mon_app_data",mon_app_data_field_list,sizeof(mon_app_data),NULL},
	{NULL, NULL}
};
void get_mon_data(mon_rec* mon_data, mon_app_data* event);
