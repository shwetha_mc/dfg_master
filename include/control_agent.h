#include "evpath.h"
#include "transport.h"
#include "ev_dfg.h"
/*EVPath handler*/
typedef int (*handler_t) (CManager cm,  void *vevent, void *client_data, attr_list attrs);

/*Control housekeeping*/
extern CManager ctrl_cm;
static EVdfg ctrl_dfg_test;

//int ctrl_init();

//int ctrl_startup();
//int ctrl_shutdown();

/*Control event types*/
/*enum ctrl_EVENT_ID
{
	ctrl_START_APP,
    ctrl_STOP_APP,
    ctrl_DESCOPE,
    ctrl_OVERSAMPLE
   //      ctrl_RECONFIGURE - for later

};
*/
/*generic control msg*/

/*
struct ctrl_event {
	enum ctrl_EVENT_ID id;
	handler_t handler;
	void *client_data;
	attr_list attrs;
};*/
/*App specific definitions start here */
/*descope by a factor of */
/*
struct ctrl_msg_descope {
	enum ctrl_EVENT_ID id;
	int factor;
	long timestamp;
};
*/
/*Reduce oversample factor by */
/*struct ctrl_msg_oversample {
	enum ctrl_EVENT_ID id;
	int reduction;
	long timestamp;
};*/
/*App specific definitions end here*/
/*
static FMField oversample_format_list[] =
{
	{""},
	{NULL,NULL}
};*/
