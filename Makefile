CC=gcc
CFLAGS=-fPIC -Wall -Wextra -Wno-unused-parameter -std=gnu99 -ggdb -g -DNVML_PRESENT -fpermissive
EVPATH_ROOT=/home/shwetha_mc/adbf2/evpath40
CW_ROOT=/home/shwetha_mc/adbf2/cwatch
CONTROL_ROOT=/home/shwetha_mc/adbf2/control
SRC_DIR=src/
BUILD_DIR=build/
INCLUDE_DIR=-Iinclude -I$(HOME)/include -I$(CW_ROOT)/src/include -I$(CW_ROOT)/build/tests -I$(EVPATH_ROOT)/build_area/evpath/source -I$(EVPATH_ROOT)/build_area/evpath/build
LIBS=-L$(HOME)/lib -L$(CW_ROOT)/build/lib -levpath -lcercs_env -latl -lffs -ldill -lpthread

#all: cod_test
all: control master

#control: $(BUILD_DIR)cw_mon.o $(BUILD_DIR)control_agent.o
#control: $(BUILD_DIR)control_agent.o
control: $(BUILD_DIR)control_agent.o $(BUILD_DIR)mon.o $(BUILD_DIR)metrics.o $(BUILD_DIR)ctrl_init.o
#$(EVPATH_ROOT)/build_area/evpath/build/CMakeFiles/evpath.dir/metrics.c.o
	$(CC) -o $(BUILD_DIR)$@ $^ $(LIBS)
#$(BUILD_DIR)cw_mon.o: src/cw_mon.c include/cw_mon.h
#	$(CC) -o $@ -c $(CFLAGS) $(INCLUDE_DIR) $<

master: $(BUILD_DIR)dfg_master.o $(BUILD_DIR)dfg_functions.o
	$(CC) -o $(BUILD_DIR)$@ $(CFLAGS) $(INCLUDE_DIR) $^ $(LIBS)
#$(BUILD_DIR)control_agent.o: src/control_agent.c src/cw_mon.c $(EVPATH_ROOT)/build_area/evpath/source/metrics.c include/control_agent.h include/cw_mon.h 

$(BUILD_DIR)ctrl_init.o: $(SRC_DIR)ctrl_init.c
	$(CC) -o $@ -c $(CFLAGS) $(INCLUDE_DIR) $^

cod_test: $(SRC_DIR)cod_test.c
	$(CC) -o $(BUILD_DIR)$@ $(CFLAGS) $(INCLUDE_DIR) $^ $(LIBS)


$(BUILD_DIR)control_agent.o: src/control_agent.c
	$(CC) -o $@ -c $(CFLAGS) $(INCLUDE_DIR) $<

$(BUILD_DIR)metrics.o: $(EVPATH_ROOT)/build_area/evpath/source/metrics.c
	$(CC) -o $@ -c $(CFLAGS) $(INCLUDE_DIR) $<

$(BUILD_DIR)mon.o: src/monitor.c 
	$(CC) -o $@ -c $(CFLAGS) $(INCLUDE_DIR) $<


$(BUILD_DIR)dfg_master.o: $(SRC_DIR)dfg_master.c
	$(CC) -o $@ -c $(CFLAGS) $(INCLUDE_DIR) $<

$(BUILD_DIR)dfg_functions.o: $(SRC_DIR)dfg_functions.c
	$(CC) -o $@ -c $(CFLAGS) $(INCLUDE_DIR) $<

.PHONY: clean

clean:
	rm -f $(BUILD_DIR)*.o $(BUILD_DIR)control





